```
module Resume.S02'StrictOptimize where
```

# Strictに関する最適化

## Haskellの評価戦略

### Call戦略

* call-by-value:

* call-by-name:

* call-by-need:

    引数を渡すときに、サンクを作成しないといけない

### パターンマッチの意味論



### seqとGHC拡張

* BangPattern
* Strict



## データ参照の共有

### WHNFとサンク

アトム(プリミティブデータ)，ラムダ抽象，コンストラクタ適用の形のデータ(1)

> If the variable concerned is bound to a weak head normal form (WHNF) - that is, an atom, lambda abstraction or constructor application ...

WHNFなデータは，Haskellでの実体を持つ実質上のアトムだと言って良い(つまりサンクで現されない，実体を持つデータ)

* アトム: プリミティブデータ
* ラムダ抽象: クロージャオブジェクト
* コンストラクタ適用: コンストラクタをタグとして持ち，フィールドが紐づくデータ

### let式と共有

* Full Laziness Transformation:

    let束縛を外部に出して，共有部分を多くする．

### 再帰と共有

* Static Argument Transformation:

    再帰部分をなるべく共有できるよう変換する．

## データ型の種類

### Strictness Flag

### Unboxed/Boxed

### Unlifted/Lifted

## 解析

* Call arity analysis: 

* Usage analysis: ？

* Cardinality Analysis:
    
    束縛変数が後の式で何回使用されているかを解析する．

### 正格性

a expression ... x ... is strict in x <=> let x = ⊥ in ... x ... = ⊥

a function f is strict in its argument <=> f x is strict in x

a let binding is strict in x <=> let x = e1 in e2 and e2 is strict in x

* safe eta-expansion

    ```haskell
    g x = \y -> error x y

    g "" `seq` ()     -- => ()
    error "" `seq` () -- => error
    ```

### Strictness Analysis

* Floating inwards:
    
    let式を内部に移動することで，strictness analysisの精度を上げる．

## Worker/Wrapper Transformation

### worker/wrapper split

### worker/wrapper transform

## リンク

1. GHCでの最適化の古典: https://www.microsoft.com/en-us/research/publication/a-transformation-based-optimiser-for-haskell/
2. Core to Coreのコンパイラの流れ: https://ghc.haskell.org/trac/ghc/wiki/Commentary/Compiler/Core2CorePipeline
3. Demand AnalysisのGHC実装の解説: https://ghc.haskell.org/trac/ghc/wiki/Commentary/Compiler/Demand
4. 最適化の順序に関するメモ: https://ghc.haskell.org/trac/ghc/wiki/Commentary/Compiler/OptOrdering
5. Demand Analysisの紹介論文: https://www.microsoft.com/en-us/research/publication/theory-practice-demand-analysis-haskell/
6. GHC 8.2.1でのDemand Analyserの改善手法紹介論文: https://pp.ipd.kit.edu/uploads/publikationen/graf17masterarbeit.pdf
7. Let floatingの紹介論文: https://www.microsoft.com/en-us/research/publication/let-floating-moving-bindings-to-give-faster-programs/
8. Cardinality Analysisの紹介論文: https://www.microsoft.com/en-us/research/publication/modular-higher-order-cardinality-analysis-in-theory-and-practice-2/
9. Projection based strictness analysisの紹介論文: https://www.researchgate.net/publication/2689227_Implementing_Projection-based_Strictness_Analysis
