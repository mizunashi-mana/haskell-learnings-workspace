{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE DerivingStrategies #-}

module TestSpace.Deriving.Via where

import Control.Monad
import GHC.TypeLits


newtype App f a = App (f a)

instance (Applicative f, Semigroup a) => Semigroup (App f a) where
    App m1 <> App m2 = App $ (<>) <$> m1 <*> m2

instance (Applicative f, Monoid a) => Monoid (App f a) where
    mempty = App $ pure mempty


data MyMaybe a = MyNothing | MyJust a
    deriving (Semigroup, Monoid) via (App MyMaybe a)
    deriving (Applicative) via (MonadDeriv MyMaybe)

deriving via (MonadDeriv MyMaybe) instance Functor MyMaybe

{-
instance (Semigroup a) => Semigroup (MyMaybe a) where
    (<>) = (coerce
      :: (App MyMaybe a -> App MyMaybe a -> App Maybe a)
      -> (MyMaybe a -> MyMaybe a -> MyMaybe a)
      ) (<>)
-}

instance Monad MyMaybe where
   return = MyJust

   MyNothing >>= _ = MyNothing
   MyJust x  >>= f = f x


newtype MonadDeriv f a = MonadDeriv (f a)
    deriving Monad via f

-- deriving newtype instance Monad f => Monad (MonadDeriv f)
-- deriving via (f) instance Monad f => Monad (MonadDeriv f)

instance Monad f => Functor (MonadDeriv f) where
    fmap = liftM

instance Monad f => Applicative (MonadDeriv f) where
    pure = return

    (<*>) = ap


{-
newtype Range :: Nat -> Nat -> Type -> Type where
    Range :: a -> Range k1 k2 a

instance (KnownNat k1, KnownNat k2, Num a, Ord a)
  => Arbitrary (Range k1 k2 a)

newtype Age = Age Int
    deriving Arbitrary via (Range 0 1000 Int)
-}