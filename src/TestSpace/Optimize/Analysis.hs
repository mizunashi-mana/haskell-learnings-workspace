module TestSpace.Optimize.Analysis where
{-# ANN module "HLint: ignore" #-}

sumInt :: [Int] -> Int
sumInt = go 0
  where
    go n []     = n
    go n (x:xs) = go (n + x) xs
