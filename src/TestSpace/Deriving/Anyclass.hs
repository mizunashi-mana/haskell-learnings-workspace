{-# LANGUAGE DeriveAnyClass #-}
{-# OPTIONS_GHC -Wno-missing-methods #-}

module TestSpace.Deriving.Anyclass where

class C a where
    cMethod :: a -> a

{-
instance C A
-}
data A = A deriving (Show, C)