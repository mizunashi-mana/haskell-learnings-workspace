{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveFunctor #-}

module TestSpace.Deriving.StrategyNotation where

newtype A a = A a
  deriving (Functor)
  deriving stock (Eq)
  deriving newtype (Ord)
  -- deriving anyclass (Show)
