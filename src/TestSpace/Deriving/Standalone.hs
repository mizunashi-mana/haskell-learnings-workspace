{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}

module TestSpace.Deriving.Standalone where


data A a = A a
-- A :: A a

deriving instance Eq a => Eq (A a)
deriving instance Ord (A Int)


data B a where
    B :: B Int

deriving instance Eq (B a)
