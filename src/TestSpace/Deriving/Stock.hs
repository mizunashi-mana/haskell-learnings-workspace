{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveTraversable #-}

module TestSpace.Deriving.Stock where

data A f a = A a (Maybe a) (f a) Int
  deriving (Show, Functor)

{-
instance Show a => Show (A a) where
    ...

instance (Functor Maybe, Functor f) => Functor (A f) where
    fmap f (A x m m2 i) = A (f x) (fmap f m) (fmap f m2) i
-}


newtype F f a = F (f a)
  deriving (Functor, Foldable, Traversable)
