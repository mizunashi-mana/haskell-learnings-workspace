{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE StandaloneDeriving #-}

module TestSpace.Deriving.Standalone2 where

import TestSpace.Deriving.Standalone


class C a where
    cMethod :: a -> a
    cMethod = id

deriving instance C (A a)
