{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module TestSpace.Deriving.Newtype where

class C a where
    cMethod :: a -> a


data A = A deriving (Show)

instance C A where
    cMethod A = A


{-
import Data.Coerce

instance C A => C NA where
    cMethod = (coerce :: (A -> A) -> (NA -> NA)) cMethod
-}
newtype NA = NA A deriving (Show, C)


newtype F f a = F (f a)
  deriving (Functor, Foldable{-, Traversable-})

{-
instance Traversable f => Traversable (F f) where
    sequenceA :: Applicative m => F f (m a) -> m (F f a)
    sequenceA = (coerce
      :: (f (m a) -> m (f a))
      -> F f (m a) -> m (F f a)) sequenceA
-}